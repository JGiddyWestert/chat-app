"""chat and message models

Revision ID: 711cd10f11f7
Revises:
Create Date: 2018-12-13 10:51:28.870797

"""
from alembic import op
import sqlalchemy as sa

# revision identifiers, used by Alembic.
revision = '711cd10f11f7'
down_revision = None
branch_labels = None
depends_on = None


def upgrade():
    op.create_table(
        'chats',
        sa.Column('id', sa.Integer(), autoincrement=True,
                  nullable=False),
        sa.Column('unit_id', sa.Integer(), nullable=False),
        sa.Column('student_id', sa.Integer(), nullable=False),
        sa.PrimaryKeyConstraint('id')
    )
    op.create_table(
        'messages',
        sa.Column('id', sa.Integer(), autoincrement=True,
                  nullable=False),
        sa.Column('chat_id', sa.Integer(), nullable=False),
        sa.Column('author_id', sa.Integer(), nullable=False),
        sa.Column('text', sa.Text(), server_default='',
                  nullable=False),
        sa.Column('created_at', sa.DateTime(), nullable=True),
        sa.ForeignKeyConstraint(['chat_id'], ['chats.id'], ),
        sa.PrimaryKeyConstraint('id')
    )


def downgrade():
    op.drop_table('messages')
    op.drop_table('chats')
