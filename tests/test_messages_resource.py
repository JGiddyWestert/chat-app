from datetime import datetime

from flask import g, url_for

from chat.models import Chat, db, Message

GET_ASSIGNED_TASK = 'chat.plugins.courses_service.get_assigned_task'
TEACHERS_BY_UNIT = 'chat.plugins.courses_service.teachers_by_unit_id'
GET_USERS = 'chat.plugins.user_service.get_users'

MOCK_STUDENT_ID = 1
MOCK_UNIT_ID = 2
MOCK_TASK_ID = 3
MOCK_TEACHER_ID = 4

MOCK_CHAT_USERS = [
    {
        'id': MOCK_STUDENT_ID,
        'first_name': 'Student_Name',
        'last_name': 'Student_Last_Name',
        'surname': 'Student_Patronymic'
    },
    {
        'id': MOCK_TEACHER_ID,
        'first_name': 'Teacher_Name',
        'last_name': 'Teacher_Last_Name',
        'surname': 'Teacher_Patronymic'
    }
]

TEACHER_AUTHOR_STR = 'Teacher_Last_Name Teacher_Name Teacher_Patronymic'
STUDENT_AUTHOR_STR = 'Student_Last_Name Student_Name Student_Patronymic'


def _create_simple_chat():
    chat = Chat(student_id=MOCK_STUDENT_ID, unit_id=MOCK_UNIT_ID)
    db.session.add(chat)
    db.session.flush()

    messages = [
        Message(chat=chat, author_id=MOCK_STUDENT_ID, text='student'),
        Message(chat=chat, author_id=MOCK_TEACHER_ID, text='teacher')
    ]

    db.session.add_all(messages)
    db.session.commit()


def test_retrieve_empty(client):
    g.user = {'id': MOCK_STUDENT_ID}
    messages_url = url_for('messages.messages', unit_id=MOCK_UNIT_ID)
    response = client.get(messages_url)
    assert response.status_code == 200
    assert len(response.json['data']) == 0


def test_retrieve(client):
    _create_simple_chat()
    g.user = {'id': MOCK_STUDENT_ID}
    messages_url = url_for('messages.messages', unit_id=MOCK_UNIT_ID)

    response = client.get(messages_url)

    assert response.status_code == 200

    message_1, message_2 = response.json['data']

    assert message_1['author'] == None
    assert message_2['author'] == None


def test_create_chat_and_message(client):
    g.user = {'id': MOCK_STUDENT_ID}
    messages_url = url_for('messages.messages', unit_id=MOCK_UNIT_ID)

        
    response = client.post(messages_url, json={
        'text': 'test message text'
    })

    assert response.status_code == 201

    created_chat = db.session.query(Chat) \
        .filter_by(unit_id=MOCK_UNIT_ID, student_id=MOCK_STUDENT_ID).one()

    created_message = db.session.query(Message) \
        .filter_by(chat_id=created_chat.id).one()

    assert created_message.text == 'test message text'
    assert created_message.author_id == MOCK_STUDENT_ID

    g.user = {'id': MOCK_TEACHER_ID}
    messages_url = url_for(
        endpoint='messages.messages',
        unit_id=MOCK_UNIT_ID,
        user_id=MOCK_STUDENT_ID
    )


    response = client.post(messages_url, json={
        'text': 'test teacher text'
    })

    assert response.status_code == 201


    response = client.post(messages_url, json={
        'text': 'test teacher text'
    })

    assert response.status_code == 201

    message_response = response.json['data']
    created_message = db.session.query(Message) \
        .get(message_response['id'])

    assert created_message.author_id == MOCK_TEACHER_ID
    assert created_message.text == 'test teacher text'

    chat = created_message.chat
    assert len(chat.messages) == 3
