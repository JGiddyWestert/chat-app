import unittest
import sys

import setuptools


with open('requirements.txt') as requirements:
    requires = [line.strip() for line in requirements.readlines()]

setuptools.setup(
    name='chat',
    version='0.0.1',
    packages=setuptools.find_packages(),
    install_requires=requires,
)