import os
from chat.utils import BaseDevConfig, BaseServiceConfig, \
    BaseTestConfig, BaseConfig


class DevelopmentConfig(BaseDevConfig, BaseServiceConfig):
    SQLALCHEMY_DATABASE_URI='sqlite:////tmp/test.db'


class TestingConfig(BaseTestConfig, BaseServiceConfig):
    SQLALCHEMY_DATABASE_URI='sqlite:////tmp/test.db'


class ProductionConfig(BaseConfig, BaseServiceConfig):
    SQLALCHEMY_DATABASE_URI='sqlite:////tmp/test.db'