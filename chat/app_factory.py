from flask import Flask

from chat.utils import authenticate
from chat.controllers.v1 import public_bp
from chat.models import db
from chat.plugins import migrate, metrics
from chat.healthcheck import database_available, add_healthcheck_handler

def create_app(config_object):
    app = Flask(__name__)
    app.config.from_object(config_object)

    db.init_app(app)
    migrate.init_app(app, db)

    app.register_blueprint(public_bp)
    app.before_request(authenticate)
    
    metrics.init_app(app)

    add_healthcheck_handler(app, [database_available])

    return app
