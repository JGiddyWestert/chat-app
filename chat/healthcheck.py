from flask import Flask, current_app
from chat.utils import jsonify

def database_available():
    flask_sqlalchemy = current_app.extensions['sqlalchemy']
    is_available = True

    try:
        flask_sqlalchemy.db.session.execute('SELECT 1;')
    except:
        is_available = False

    return is_available


def add_healthcheck_handler(app: Flask, healthchecks: list,
                            url='/health'):
    """Add heath handler for external monitoring.

    >>> from flask_utils import healthchecks as hc

    >>> app = Flask(__name__)
    >>> app.config.from_object(config_object)

    >>> hc.add_healthcheck_handler(app, [
    ...    hc.database_available,
    ...    hc.redis_available,
    ... ])

    >>>
    """
    def healthcheck_handler():
        http_status = 200
        result = {
            check_func.__name__: check_func()
            for check_func in healthchecks
        }

        if not all(result.values()):
            http_status = 500

        return jsonify(result,http_status)

    app.add_url_rule(rule=url, methods=['GET'], view_func=healthcheck_handler)